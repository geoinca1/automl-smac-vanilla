import pandas as pd
import numpy as np
import sys

def extract_optimization_time_group(df):
    number_of_groups = df.optimization_times_per_group.apply(lambda x : x.keys()).apply(len).unique()[0]
    df['optimization_time_group_0'] = df.optimization_times_per_group.apply(lambda x: x['0']).apply(sum)
    for i in range(1, number_of_groups):
        df['optimization_time_group_' + str(i)] = df.optimization_times_per_group.apply(lambda x: x[str(i)]).apply(sum) + df['optimization_time_group_' + str(i-1)]
    return df

def extract_scores_group(df):
    number_of_groups = df.optimization_times_per_group.apply(lambda x : x.keys()).apply(len).unique()[0]
    for i in range(0, number_of_groups):
        df['test_score_group_' + str(i)] = df.candidates_per_group.apply(lambda x: x[str(i)]).apply(lambda x : [i['test_score'] for i in x if i['test_score'] != None]).apply(np.max)
        df['valid_score_group_' + str(i)] = df.candidates_per_group.apply(lambda x: x[str(i)]).apply(lambda x : [i['score'] for i in x if i['score'] != None and i['score'] <= 1]).apply(np.mean)
    return df

if __name__ == '__main__':
    input=sys.argv[1]
    output=sys.argv[2]
    df = pd.read_parquet(input, 'fastparquet')
    df = extract_optimization_time_group(df)
    df = extract_scores_group(df)
    df.to_parquet(output, 'fastparquet', 'gzip')
