#!/bin/bash

dataset=$1

mkdir -p datasets

if [ "${dataset}" == "covertype" ]
then
  wget https://www.openml.org/data/get_csv/2418/covtype-normalized.arff -O datasets/covertype.csv
elif [ "${dataset}" == "miniboone" ]
then
  wget 'https://www.openml.org/data/get_csv/19335523/MiniBooNE.arff' -O datasets/miniboone.csv
else
  echo "Warning, the dataset has ${dataset} has not been found to be downloaded"
fi
