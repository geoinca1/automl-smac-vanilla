import os
import fnmatch
import argparse

import pandas as pd


def dataset_results(directory='.'):
    files = []

    for file in os.listdir(directory):
        if fnmatch.fnmatch(file, '*.json'):
            files.append(file)

    return files

def merge_results(result_files, directory='.'):
    if not result_files:
        raise Exception('There is no files')

    df = pd.read_json(directory + result_files.pop())

    for result_file in result_files:
        other = pd.read_json(directory + result_file)
        df = df.append(other, ignore_index=True, sort=False)

    return df

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merge results')

    parser.add_argument(
        '--result-directory',
        dest="result_directory",
        default="./",
        type=str,
    )

    parser.add_argument(
        '--output-file',
        default='result.parquet',
        dest='output_file',
        type=str,
    )

    args = parser.parse_args()

    print('Retrieve files')
    results = dataset_results(directory=args.result_directory)

    print('Merge results')
    df = merge_results(results, args.result_directory)

    print('Save results')
    df.to_parquet(args.output_file, 'fastparquet', 'gzip')
