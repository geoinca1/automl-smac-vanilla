import yaml
import pandas
import importlib

def get_dataset(dataset, dataset_mapper):
    with open(dataset_mapper, 'r') as stream:
        try:
            datasets = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    if dataset not in datasets:
        raise Exception("Dataset {} does not exist in datasets.yml".format(dataset))

    backend = datasets[dataset]['backend']

    if backend == "local":
        dataframe = read_dataframe_from_csv(dataset)
    elif backend == "scikit":
        function_to_call = datasets[dataset]['function']
        return read_dataframe_from_scikit(function_to_call)
    else:
        raise Exception("Unknown backend {}".format(backend))

    column = 'class'
    if 'label' in datasets[dataset]:
        column = datasets[dataset]['label']

    data, target = extract_data(dataframe, column)

    return data, target

def read_dataframe_from_scikit(function_to_call):
    skd = importlib.import_module('sklearn.datasets')
    data = getattr(skd, function_to_call)()
    return data.data, data.target

def read_dataframe_from_csv(dataset):
    return pandas.read_csv("datasets/{}.csv".format(dataset))

def extract_data(dataframe, label_column):
    if not label_column in dataframe.columns:
        label_column = dataframe.columns[-1]

    dataframe = transform_str_features_to_int(dataframe)

    return dataframe.drop(columns=[label_column]).values, dataframe[label_column].values

def transform_str_features_to_int(dataframe): # FIXME not a good solution, should be OHE instead
    for column in dataframe.columns:
        if dataframe[column].dtype == 'O': # NOTE 'O' is equivalent to object
            values = dataframe[column].unique().tolist()
            mapping = dict(zip(values, range(len(values))))
            dataframe.replace({column: mapping}, inplace=True)
    return dataframe
