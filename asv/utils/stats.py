import time
import pandas as pd
from collections import defaultdict

class SingletonDecorator:
    def __init__(self, klass):
        self.klass = klass
        self.instance = None
    def __call__(self, *args, **kwds):
        if self.instance == None:
            self.instance = self.klass(*args, **kwds)
        return self.instance

@SingletonDecorator
class Stats(object):
    def __init__(self, number_of_candidates_per_group=100):
        self.counter = 0
        self.number_of_candidates_per_group = number_of_candidates_per_group
        self.candidates_per_group = defaultdict(list)
        self.initial_optimization_time = None
        self.last_time = None
        self.optimization_times_per_group = defaultdict(list)
        self.extra_test_time_per_group = defaultdict(list)

    def initial_optimization_timer(self):
        self.last_time = time.time()

    def stop_optimization_timer(self):
        current_time = time.time()
        self.optimization_times_per_group[self._compute_current_group()-1].append((current_time - self.last_time))

    def _compute_current_group(self):
        return int(self.counter / self.number_of_candidates_per_group)

    def _compute_current_optimization_time(self):
        current_time = time.time()
        result = current_time - self.last_time
        self.last_time = current_time
        return result


    def append_candidate(self, name, score, test_score, status, extra_test_time):
        current_group = self._compute_current_group()

        self.candidates_per_group[current_group].append({'name': name, 'score': score, 'test_score': test_score, 'status': status})

        self.extra_test_time_per_group[current_group].append(extra_test_time)
        self.optimization_times_per_group[current_group].append(self._compute_current_optimization_time())

        self.counter += 1

    def toDF(self):
        return pd.DataFrame(data=[[self.optimization_times_per_group, self.candidates_per_group, self.extra_test_time_per_group]],
            columns=['optimization_times_per_group', 'candidates_per_group', 'extra_test_time_per_group'])

    def __str__(self):
        return str(self.toDF())

    def saveToJson(self, filename=None):
        if not filename:
            filename = self.benchmark_file
        self.toDF().to_json(filename)
